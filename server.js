const express = require('express');
var cors = require('cors');
const app = express();

app.use(cors({
    origin: '*'
}));

app.get('/books', (req, res) => {
    const host = req.get('host');
    const origin = req.get('origin');
    // console.log("host", host)
    console.log("origin", origin)
    // var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
    // console.log("ip", ip)
    // if (origin !== "http://13.212.103.191:8081") {
    //     res.status(401).send()
    //     return false;
    // }

    let data = [{
        "id": "1",
        "name": "Game of thrones"
    },
    {
        "id": "2",
        "name": "Clash of kings"
    }]
    res.json(data);
})

app.listen(8080, () => {
    console.log('Start server at port 8080.')
})